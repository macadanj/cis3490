#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<math.h>
#include"tool.h"
/*
 * Name: Josh Ryan Macadangdang
 * Course: CIS 3490
 * Date Created: Feb 17, 2021
 * Last Updated: Feb 21.2021
 * Description: Solves the convex hull problem using Graham Scan.
 * Code reference: https://en.wikipedia.org/wiki/Graham_scan
 *                 https://www.geeksforgeeks.org/orientation-3-ordered-points/
*/

#define MAX_SIZE 30000

Point p0;
void swap( Point *a, Point *b ) {
    Point temp = *a;
    *a = *b;
    *b = temp;
}

int distSq(Point p1, Point p2) 
{ 
    return (p1.x - p2.x)*(p1.x - p2.x) + 
          (p1.y - p2.y)*(p1.y - p2.y); 
} 
  
// A function used by library function qsort() to sort an array of 
// points with respect to the first point 
int compare(const void *vp1, const void *vp2) 
{ 
   Point *p1 = (Point *)vp1; 
   Point *p2 = (Point *)vp2; 
  
   // Find orientation 
   int o = orientation(p0, *p1, *p2); 
   if (o == 0) 
     return (distSq(p0, *p2) >= distSq(p0, *p1))? -1 : 1; 
  
   return (o == 2)? -1: 1; 
}

int compareX(const void *vp1, const void *vp2) {
    Point *p1 = (Point *)vp1; 
    Point *p2 = (Point *)vp2;  
    if (p1->x == p2->x)
        return (int)ceil(p2->y - p1->y); 
    return (int)ceil(p2->x - p1->x);
}

void grahamScan( Point points[], int n ) {
    //Find the bottom-most point.
    int ymin = points[0].y, min = 0;
    for (int i = 1; i < n; i++) {
        int y = points[i].y;
        if((y < ymin) || (ymin == y && points[i].x < points[min].x)) {
            ymin = points[i].y;
            min = i;
        }
    }

    swap(&points[0], &points[min]);

    //Sort points by polar angle with P0, 
    //if several points have the same polar angle then only keep the farthest.
    p0 = points[0];
    qsort(&points[1], n-1, sizeof(Point), compare); 

    int m = 1;
    for (int i = 1; i < n; i++) {
        // Keep removing i while angle of i and i+1 is colinear
        // with respect to p0 
        while (i < n-1 && orientation(p0, points[i], points[i+1]) == 0) 
            i++; 
        points[m] = points[i];
        m++;
    }
    if (m < 3) return;

    Stack *stack = createStack(m);
    push(stack, points[0]);
    push(stack, points[1]);
    push(stack, points[2]);
    //Process the remaining n-3 points
    for (int i = 3; i < m; i++) {
        while(orientation(nextToTop(stack), peek(stack), points[i]) != 2) 
            pop(stack);
        push(stack, points[i]);
    }
   
    qsort(&stack->array[0], stack->top + 1, sizeof(Point), compareX); 
    printf("Number of points = %d\n", stack->top + 1);
    //Print contents of the stack .
    int i = 0;
    while (!isEmpty(stack)) {
        Point p = peek(stack);
        printf("%d. (%.1f, %.1f)\n", i + 1, p.x, p.y);
        pop(stack);
        i++;
    }

}

int main( int argc, char *argv[] ) {
    if (argc < 2) {
        printf("Usage: ./bin/P3 [filename]\n");
        return -1;
    }    Point points[MAX_SIZE];
    FILE *fp = fopen(argv[1], "r");
    if (fp == NULL) {
        printf("Error file not found.\n");
    }
    float x = 0;
    float y = 0;
    int i = 0;
    fscanf(fp,"%f", &x);
    while (!feof(fp)) {
        fscanf(fp,"%f", &y);
        points[i] = *createPoint(x,y);
        fscanf(fp,"%f", &x);
        i++;
    } 
    fclose(fp);
    int n = sizeof(points)/sizeof(points[0]); 
    clock_t begin = clock();
    grahamScan(points, n);
    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf( "Execution Time: %fs\n", time_spent );
    return 0;
}