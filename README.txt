Josh Ryan Macadangdang - 1097054
Assignment 2

P1 - program determines the number of inversions in an array of positive integers.
usage: ./bin/P1 [name of file]

P2 - program to solve the convex hull problem using the brute force algorithm Jarvis MArch. 
usage: ./bin/P2 [name of file]

P3 - program to solve the convex hull problem using the divide and conquer algorithm Graham Scan. 
usage: ./bin/P3 [name of file]

