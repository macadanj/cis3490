#include<limits.h>
#include<stdio.h>
#include<stdlib.h>
typedef struct Point 
{ 
    float x, y; 
} Point;

typedef struct Stack
{
    int top;
    unsigned capacity;
    Point *array;
    /* data */
} Stack;

//Point functions
Point *createPoint(float x, float y) {
    Point *point = (Point *)malloc(sizeof(Point));
    point->x = x;
    point->y = y;
    return point;
}

int isEqual( Point p1, Point p2 ) {
    if (p1.x == p2.x && p1.y == p2.y) return 1;
    return 0;
}

int orientation(Point p, Point q, Point r) 
{ 
  int val = (q.y - p.y) * (r.x - q.x) - 
              (q.x - p.x) * (r.y - q.y); 
  
    if (val == 0) return 0;  // colinear 
    return (val > 0)? 1: 2; // clock or counterclock wise 
}

//Stack functions
Stack *createStack(unsigned capacity) {
    Stack* stack = (Stack*)malloc(sizeof(struct Stack));
    stack->capacity = capacity;
    stack->top = -1;
    stack->array = (Point*)malloc(stack->capacity * sizeof(Point));
    return stack;
}

int isFull(Stack *stack) {
    return stack->top == stack->capacity - 1;
}
int isEmpty(Stack *stack) {
    return stack->top == -1;
}
void push(Stack *stack, Point item) {
    if(isFull(stack)) return;
    stack->array[++stack->top] = item;
    //printf("(%d,%d) pushed to stack\n", item.x, item.y);

}
Point pop(Stack *stack) {
    Point result;
    result.x = 0;
    result.y = 0;
    if(isEmpty(stack)) return result;
    return stack->array[stack->top--];
}
Point peek(Stack *stack){
    Point result;
    result.x = 0;
    result.y = 0;
    if(isEmpty(stack)) return result;
    return stack->array[stack->top];
}

Point nextToTop(Stack *stack) {
    Point top = peek(stack);
    pop(stack);
    Point result = peek(stack);
    push(stack, top);
    return result;
}
