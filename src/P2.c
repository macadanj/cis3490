#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<math.h>
#include"tool.h"
/*
 * Name: Josh Ryan Macadangdang
 * Course: CIS 3490
 * Date Created: Feb 17, 2021
 * Last Updated: Feb 21.2021
 * Description: Solves the convex hull problem using Jarvis March.
 * Code reference: https://en.wikipedia.org/wiki/Gift_wrapping_algorithm
 *                 https://www.geeksforgeeks.org/orientation-3-ordered-points/
*/

#define MAX_SIZE 30000

int compare(const void *vp1, const void *vp2) {
    Point *p1 = (Point *)vp1; 
    Point *p2 = (Point *)vp2; 
    if (p1->x == p2->x)
        return (int)ceil(p1->y - p2->y);
    return (int)ceil(p1->x - p2->x);
}

void jarvis( Point points[], int n ) {
    // There must be at least 3 points.
    if (n < 3) return; 
  
    // Initialize Result 
    Point hull[n]; 
  
    Point pointOnHull; //leftmost point in points[].
    Point endpoint;
    int l = 0;
    //Find the left most points in points[].
    for( int i = 0; i < n; i++ ) {
        if ( points[i].x < points[l].x ) { 
            l = i;
        }
    }
    pointOnHull = points[l];
    int i = 0;
    int q;
    do {
        hull[i] = pointOnHull;
        q = (l + 1)%n;
        endpoint = points[q]; 
        for ( int j = 0; j < n; j++ ) {
            if ( orientation( pointOnHull, points[j], endpoint ) == 2 ) {
                endpoint = points[j];
            }
        }
        i = (i + 1)%n;
        pointOnHull = endpoint;
    } while ( !isEqual( endpoint, hull[0] ) );

    printf("Number of points = %d\n", i);
    qsort(&hull[0], i, sizeof(Point), compare); 

    for ( int k = 0; k < i; k++ ) {
        printf("%d. (%.1f, %.1f)\n", k + 1, hull[k].x, hull[k].y );
    }
}

int main( int argc, char *argv[] ) {
    if (argc < 2) {
        printf("Usage: ./bin/P2 [filename]\n");
        return -1;
    }    Point points[MAX_SIZE];
    FILE *fp = fopen(argv[1], "r");
    if (fp == NULL) {
        printf("Error file not found.\n");
    }
    float x = 0;
    float y = 0;
    int i = 0;
    fscanf(fp,"%f", &x);
    while (!feof(fp)) {
        fscanf(fp,"%f", &y);
        points[i] = *createPoint(x,y);
        fscanf(fp,"%f", &x);
        i++;
    } 
    fclose(fp);
    int n = sizeof(points)/sizeof(points[0]); 
    clock_t begin = clock();
    jarvis(points, n); 
    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf( "Execution Time: %fs\n", time_spent );
    return 0; 
}