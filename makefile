CC = gcc
FLAGS = -std=c99 -Wall -pedantic -Iinclude
BIN = ./bin/
SRC = ./src/
INC = ./include/

PROG = P1 P2 P3
TEST = stackTest
HEAD = tool.h
LIST = $(addprefix $(BIN)/, $(PROG))
TEST_LIST = $(addprefix $(BIN)/, $(TEST))
LIBS = $(addprefix $(INC)/, $(HEAD))

all: $(LIST)

test: $(TEST_LIST)

$(BIN)/%: $(SRC)%.c $(LIBS)
	$(CC) $(FLAGS) $< -o $@ $(LIBS) -lm

clean: 
	rm -rf $(LIST) $(TEST_LIST)