#include<stdio.h>
#include<time.h>
#include"tool.h"
/*
 * Name: Josh Ryan Macadangdang
 * Course: CIS 3490
 * Date Created: Feb 17, 2021
 * Last Updated: Feb 17.2021
 * Description: Determines the number of inversions in an array.
 * Code references: https://www.geeksforgeeks.org/counting-inversions/
 *                  https://stackoverflow.com/questions/4600797/read-int-values-from-a-text-file-in-c
 *                  https://stackoverflow.com/questions/5248915/execution-time-of-c-program#:~:text=To%20get%20the%20CPU%20time,as%20a%20floating%20point%20type.
*/

#define MAX_SIZE 50000

//Merge and sort an array and return the number of inversions.
int merge(int arr[], int temp[], int left, int mid, int right) {
    int result = 0;
    int i, j, k;
    i = left;
    j = mid;
    k = left;

    while ((i <= mid - 1) && (j <= right)) {
        if (arr[i] <= arr[j]) {
            temp[k++] = arr[i++];
        } else {
            temp[k++] = arr[j++];
            result = result + (mid - i);
        }
    }

    //Copy remaining elements of left subarray (if any) onto temp.
    while (i <= mid - 1) temp[k++] = arr[i++];

    //Copy remaining elements of right subarray (if any) onto temp.
    while(j <= right) temp[k++] = arr[j++];

    //Copy the merged elements back into the original array.
    for (i = left; i <=right; i++) {
        arr[i] = temp[i];
    }

    return result;
}

//Brute force algoritm to calculate number of inversions in an array of integers.
int solve1(int arr[], int n) {
    int result = 0;
    for (int i = 0; i < n-1; i++) {
        for (int j = i+1; j < n; j++) {
            if (arr[i] > arr[j]) result++;
        }
    }
    return result;
}

//Recursive algorithm to to calculate number of inversions in an array of integers.
int solve2(int arr[], int temp[], int l, int r) {
    int result = 0;
    if (l < r ) {
        int m = (l + r) / 2;
        result += solve2(arr, temp, l, m );
        result += solve2(arr, temp, m+1, r);
        result += merge(arr,temp, l, m+1, r);
    }
    return result;
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        printf("Usage: ./bin/P1 [filename]\n");
        return -1;
    }
    FILE *fp;
    fp = fopen(argv[1], "r");
    if (fp == NULL) {
        printf("Error: file not found.\n");
        return -1;
    }
    int arr[MAX_SIZE];
    int i = 0;
    int j = 0;
    fscanf (fp, "%d", &i);
    while (!feof(fp)) {
        arr[j] = i;
        fscanf (fp, "%d", &i); 
        j++;
    }
    fclose(fp);

    int n = sizeof(arr) / sizeof(int);
    int temp[n];

    clock_t begin = clock();
    printf("Brute Force: Num of inversions = %d\t", solve1(arr, n));
    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("Brute Force: Execution Time: %fs\n", time_spent);

    begin = clock();
    printf("Divide and Conquer: Num of inversions = %d\t", solve2(arr, temp, 0, n-1));
    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("Divide and Conquer: Execution Time: %fs\n", time_spent);

    return 0;
}